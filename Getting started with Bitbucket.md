# [Getting started with Bitbucket](https://confluence.atlassian.com/display/BITBUCKET/Getting+started+with+Bitbucket)


Created by [Dan Stevens [Atlassian]](https://confluence.atlassian.com/display/~dstevens), last modified on Oct 15, 2014

Learn how to set up your account, repositories, import code, convert from SVN, and the basics of Git and Bitbucket.

Starting out using a new version control system (VCS) can be intimidating we hope to make the process of using Bitbucket as painless as possible. This section is intended to help you get set up and started with Bitbucket for a complete walk through see our tutorials: 

- [Bitbucket 101][0] Git with branches
- [Bitbucket 201 Bitbucket with Git and Mercurial][1]

[0]: https://confluence.atlassian.com/display/BITBUCKET/Bitbucket+101
[1]: https://confluence.atlassian.com/display/BITBUCKET/Bitbucket+201+Bitbucket+with+Git+and+Mercurial



## This section contains 

> [Create a repository][2]
  Learn the basics of how to create a repository in Bitbucket

> [Hackathon participants][3]
  A quick start guide for people using Bitbucket as a Git repository for competitions.

> [Import code from an existing project][4]
  Learn how to grab code from another project and put it into your Bitbucket repository.

> [Convert from other version control systems][5]
  Learn how to convert your existing version control

> [Keyboard shortcuts][6]
  Learn how to access and use keyboard shortcuts in Bitbucket.

[2]: https://confluence.atlassian.com/display/BITBUCKET/Create+a+repository
[3]: https://confluence.atlassian.com/display/BITBUCKET/Hackathon+participants
[4]: https://confluence.atlassian.com/display/BITBUCKET/Import+code+from+an+existing+project
[5]: https://confluence.atlassian.com/display/BITBUCKET/Convert+from+other+version+control+systems
[6]: https://confluence.atlassian.com/display/BITBUCKET/Keyboard+shortcuts


## [6 Child Pages][7]

Page1: [Keyboard shortcuts][8]   
Page2: [Create a repository][9]   
Page3: [Hackathon participants][10]   
Page4: [Import code from an existing project][11]   
Page5: [Sourcetree Git GUI on Windows][12]   
Page6: [SourceTree a Free Git and Mercurial GUI (Mac OSX)][13]   

[7]: https://confluence.atlassian.com/display/BITBUCKET/Getting+started+with+Bitbucket?showChildren=false#children
[8]: https://confluence.atlassian.com/display/BITBUCKET/Keyboard+shortcuts
[9]: https://confluence.atlassian.com/display/BITBUCKET/Create+a+repository
[10]: https://confluence.atlassian.com/display/BITBUCKET/Hackathon+participants
[11]: https://confluence.atlassian.com/display/BITBUCKET/Import+code+from+an+existing+project
[12]: https://confluence.atlassian.com/display/BITBUCKET/Sourcetree+Git+GUI+on+Windows
[13]: https://confluence.atlassian.com/pages/viewpage.action?pageId=269981808

